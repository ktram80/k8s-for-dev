## K8S Install

I'm using three VMs (Ubuntu 20.04, 4GiB RAM), one for the control plane and two for the worker nodes. The following steps are required for both control and worker nodes.

### From step 1 to 6, all commands need to be run on all nodes.

### step 1: enable kernel modules
```
cat << EOF | sudo tee /etc/modules-load.d/containerd.conf
overlay
br_netfilter
EOF
```
```
sudo modprobe overlay
sudo modprobe br_netfilter
```

### step 2: network settings
```
cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF
```
```
sudo sysctl --system
```

### step 3: install containerd package
```
sudo apt-get update && sudo apt-get install -y containerd
```

### step 4: containerd config
```
sudo mkdir -p /etc/containerd
```
```
sudo containerd config default | sudo tee /etc/containerd/config.toml
```
```
sudo systemctl restart containerd
```

### step 5: disable swap
```
sudo swapoff -a
```

### step 6: install kubernetes packages
```
sudo apt-get update && sudo apt-get install -y apt-transport-https curl
```
```
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
```
```
cat << EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
```
```
sudo apt-get update
sudo apt-get install -y kubelet=1.23.0-00 kubeadm=1.23.0-00 kubectl=1.23.0-00
```
```
sudo apt-mark hold kubelet kubeadm kubectl
```

### step 7: initialize k8s cluster (executed on control node)
```
sudo kubeadm init --pod-network-cidr 192.168.0.0/16 --kubernetes-version 1.23.0

mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

### step 8: k8s network setup
```
kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
```

### step 9: create the 'join command' for the worker nodes (executed on control node)
```
kubeadm token create --print-join-command
```

### step 10: join worker nodes (executed on each worker node)
#### copy and paste the printed result of step 9
```
sudo kubeadm join <control-node-ip>:6443 --token <generated token> --discovery-token-ca-cert-hash <hashcode>
```

### verify (on control node)
```
kubectl get nodes
NAME      STATUS   ROLES                  AGE    VERSION
control   Ready    control-plane,master   124m   v1.23.0
worker1   Ready    <none>                 116m   v1.23.0
worker2   Ready    <none>                 116m   v1.23.0
```