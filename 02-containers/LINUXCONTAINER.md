# linux container demo

## Creating a sandbox (a.k.a. container)

### step 0
```
su -
```
### step 1
Create a new user space (a new directory structure that will be included in the sandbox):

The example here is this tarball (alpinepy.tar), with the directory structure of a linux distro and the necessary dependencies to run a python program that we will use in the example

### step 2
Create a work directory and within it create a directory called "rootfs" (it can be any name, I used this one because it is more didactic)
```
mkdir -p <workdir>/rootfs
```
### step 3
Extract the alpinepy.tar tarball in the rootfs directory (it will be the sandbox filesystem)
```
tar xvf alpinepy.tar -C rootfs/
```
### step 4
Create a new control group for our experiment (memory restriction used by the sandbox)
```
mkdir -p /sys/fs/cgroup/memory/demo

echo "100000000" > /sys/fs/cgroup/memory/demo/memory.limit_in_bytes

echo "0" > /sys/fs/cgroup/memory/demo/memory.swappiness
```
### step 5
Create a new namespace to isolate the processes (PID) that will be used in the sandbox
```
unshare --fork --pid --mount-proc /bin/bash
```
### step 6
Complete the configuration of the control group, placing the new PID tree in the list of restrictions
```
echo $$ > /sys/fs/cgroup/memory/demo/tasks
```
(if you want to check: cat /sys/fs/cgroup/memory/demo/tasks)

### step 7
mount the proc and dev directories
```
mount --bind /proc/ rootfs/proc/
mount --bind /dev/ rootfs/dev/
```
### step 8
Change root for the new sandbox filesystem
```
chroot rootfs /bin/sh
```
### step 9
Test the memory leak program (has a 100mb ram limit)
```
python /usr/local/bin/mem_leak.py 
10mb
20mb
30mb
40mb
50mb
60mb
70mb
80mb
Killed
```
### step 10
Cleanup
```
umount rootfs/proc
umount rootfs/dev
exit # (get out of the namespace)
exit # (get out of the root)
```
