# Kubernetes, for software developers 

A set o tips for developers when using Kubernetes  

## Getting started

I hope this repository can be used as a guide for anyone looking to design, build, configure and expose cloud-native applications for Kubernetes.

## Table of Contents
- [Kubernetes install](./01-k8s-install/README.md)
- [About Containers](./02-containers/LINUXCONTAINER.md)
### (more content coming soon)